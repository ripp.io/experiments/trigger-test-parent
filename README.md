# Pipeline Trigger Parent

This experiment is to get a slim working example of a parent and child pipeline across repos, sharing environment variables and artifacts.

See [child repo](https://gitlab.com/ripp.io/experiments/trigger-test-child) for child pipeline.

```mermaid
stateDiagram-v2
    direction LR
    
    state parent {
        direction LR
        [*] --> build_artifact
        retrieve_results --> [*]
    }

    state child {
        direction LR
        [*] --> download_artifact
        download_artifact --> [*]
    }

    build_artifact --> child
    child --> retrieve_results
```
